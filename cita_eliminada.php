<?php 
  session_start();
  $matricula = $_SESSION["matricula"];

	//connexio a la base de dades
	//$prueba = mysqli_connect("localhost", "root", "", "JAL_ITV");
	//connexio a la base de dades de labs
    $prueba = mysqli_connect("localhost", "a14larpetpet_jal", "123456", "a14larpetpet_ITV");

	//comprueba la conexion a la BD
	if(mysqli_connect_errno()){
		die("ERROR: No s'ha pogut connectar. " . mysqli_connect_error());
	}
  //elimina la cita de la base de dades
  $sql = "DELETE FROM Cita WHERE matricula = '$matricula'";
  if(mysqli_query($prueba, $sql)){
    $missatge = "La teva cita s'ha eliminat correctament!";
  }
  else{
    $missatge = "ERROR: " . mysqli_error($prueba);
  }
  // cerrar conexion
	mysqli_close($prueba);
?>
<!DOCTYPE html>
<html>
<head>
	<title>Motors IAM</title>
</head>
<body>
	<?php include 'php/header.php';?>
	<script type="text/javascript" src="js/cita.js"></script>
	<div class="body">
    <section class="missatge">
      <h3>
        <?php echo $missatge?>
      </h3>
    </section>
	</div>
	<?php include 'php/footer.php';?>
</body>
</html>