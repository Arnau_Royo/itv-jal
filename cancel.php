<!DOCTYPE html>
<html>
<head>
	<title>Motors IAM</title>
	<link rel="stylesheet" type="text/css" href="css/cancel.css">
	<meta http-equiv='refresh' content="5;url=index.php">
</head>
<body>
	<?php include 'php/header.php';?>
	<script type="text/javascript" src="js/header.js"></script>
	<div class="body">
		<div id="missatge">
			<h3>S'ha cancel·lat la vostra reserva</h3>
			<h4>Tornant a la pàgina principal...</h4>
		</div>	
	</div>

	
	<?php include 'php/footer.php';?>
</body>
</html>