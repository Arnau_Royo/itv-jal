-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 22, 2017 at 05:45 PM
-- Server version: 5.7.20-0ubuntu0.16.04.1
-- PHP Version: 7.0.22-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ITV_JAL`
--

-- --------------------------------------------------------

--
-- Table structure for table `Cita`
--

CREATE TABLE `Cita` (
  `Dia` date NOT NULL,
  `Hora` time NOT NULL,
  `matricula` varchar(15) COLLATE ucs2_spanish_ci NOT NULL,
  `codi_cita` int(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=ucs2 COLLATE=ucs2_spanish_ci;

--
-- Dumping data for table `Cita`
--

INSERT INTO `Cita` (`Dia`, `Hora`, `matricula`, `codi_cita`) VALUES
('2017-12-30', '19:00:00', '1234LLL', 1),
('2017-12-25', '10:30:00', '1234CCC', 2),
('2017-12-26', '16:30:00', '3214FFF', 3);

-- --------------------------------------------------------

--
-- Table structure for table `Client`
--

CREATE TABLE `Client` (
  `Nom` varchar(20) COLLATE ucs2_spanish_ci NOT NULL,
  `Cognoms` varchar(20) CHARACTER SET ucs2 COLLATE ucs2_spanish2_ci NOT NULL,
  `Email` varchar(40) CHARACTER SET ucs2 COLLATE ucs2_spanish2_ci NOT NULL,
  `Telefon` varchar(20) COLLATE ucs2_spanish_ci NOT NULL,
  `codi_client` int(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=ucs2 COLLATE=ucs2_spanish_ci;

--
-- Dumping data for table `Client`
--

INSERT INTO `Client` (`Nom`, `Cognoms`, `Email`, `Telefon`, `codi_client`) VALUES
('Arnau', 'Royo Raso', 'a16arnroyras@iam.cat', '608792601', 1),
('Fernanda', 'del Rocío', 'ferlachoni@gmail.com', '678400568', 2),
('Prima', 'del Anime', 'omaewamo@gmail.com', '678400168', 3);

-- --------------------------------------------------------

--
-- Table structure for table `Vehicle`
--

CREATE TABLE `Vehicle` (
  `Tipus` varchar(25) COLLATE ucs2_spanish_ci NOT NULL DEFAULT 'Cotxe',
  `matricula` varchar(15) COLLATE ucs2_spanish_ci NOT NULL,
  `data` datetime NOT NULL,
  `codi_vehicle` int(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=ucs2 COLLATE=ucs2_spanish_ci;

--
-- Dumping data for table `Vehicle`
--

INSERT INTO `Vehicle` (`Tipus`, `matricula`, `data`, `codi_vehicle`) VALUES
('Cotxe', '1234CCC', '2017-12-25 10:30:00', 2),
('Cotxe', '1234LLL', '2017-12-30 19:00:00', 1),
('Cotxe', '3214FFF', '2017-12-26 16:30:00', 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Cita`
--
ALTER TABLE `Cita`
  ADD PRIMARY KEY (`codi_cita`),
  ADD KEY `matricula` (`matricula`);

--
-- Indexes for table `Client`
--
ALTER TABLE `Client`
  ADD PRIMARY KEY (`codi_client`);

--
-- Indexes for table `Vehicle`
--
ALTER TABLE `Vehicle`
  ADD PRIMARY KEY (`matricula`),
  ADD UNIQUE KEY `codi_client` (`codi_vehicle`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Cita`
--
ALTER TABLE `Cita`
  MODIFY `codi_cita` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `Client`
--
ALTER TABLE `Client`
  MODIFY `codi_client` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
