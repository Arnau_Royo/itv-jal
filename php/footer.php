<footer>
	<div class="links">
		<p><a href="admin.php">Login administrador</a></p>
		<p><a href="guia_estil.php">Guia d'estil</a></p>
	</div>
	<div class="wrapper">
		<ul>
			<li class="facebook"><i class="fa fa-facebook fa-2x" aria-hidden="true"></i></li>
			<li class="twitter"><i class="fa fa-twitter fa-2x" aria-hidden="true"></i></li>
			<li class="instagram"><i class="fa fa-instagram fa-2x" aria-hidden="true"></i></li>
			<li class="google"><a href="http://www.google.com"><i class="fa fa-google fa-2x" aria-hidden="true"></i></a></li>
			<li class="whatsapp"><i class="fa fa-whatsapp fa-2x" aria-hidden="true"></i></li>
		</ul>
	</div>
	<p id="license">Copyright &copy;</p>
</footer>