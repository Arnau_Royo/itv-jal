<?php 
	session_start(); 
  $matricula = $_SESSION["matricula"];
	//se conecta al servidor	
	//$prueba = mysqli_connect("localhost", "root", "", "JAL_ITV"); 
	//connexio a la base de dades de labs
	$prueba = mysqli_connect("localhost", "a14larpetpet_jal", "123456", "a14larpetpet_ITV");

	//comprueba la conexion a la BD
	if(mysqli_connect_errno()){
		die("ERROR: No s'ha pogut connectar. " . mysqli_connect_error());
	}
	$sql = "SELECT Dia, Hora
          FROM Client INNER JOIN Cita ON Client.codi_client = Cita.codi_cita ";
  $resultat = mysqli_query($prueba, $sql);

  if(mysqli_num_rows($resultat) > 0){
		while($fila = mysqli_fetch_assoc($resultat)){
			$resul = $fila["Dia"];
			$resul2 = $fila["Hora"];
		}
	}
	
// cerrar conexion
	mysqli_close($prueba);

?>
<!DOCTYPE html>
<html>
<head>
	<title>Motors IAM</title>
</head>
<body>
	<?php include 'php/header.php';?>
	<script type="text/javascript" src="js/cita.js"></script>
	<div class="body">
		<section class="cita">
			<h2>Cita ja existent!</h2>
			<p>El cotxe amb la matricula <strong><?php echo $matricula?></strong> te cita per el dia <strong><?php echo $resul ?></strong> a la hora <strong><?php echo $resul2 ?></strong> .</p>
      <form method="POST" action="cita_eliminada.php"  style="float: left">
				<input type="submit" name="eliminar_cita" value="Eliminar cita">
			</form>
			<form method="POST" action="cita_modificada.php">
				<input type="submit" name="modificar_cita" value="Modificar cita">
			</form>
			<form method="POST" action="index.php">
				<input type="submit" name="modificar_cita" value="Cancelar">
			</form>
		</section>
	</div>
	<?php include 'php/footer.php';?>
</body>
</html>