<!DOCTYPE html>
<html>
<head>
	<title>Motors IAM</title>
	<link rel="stylesheet" type="text/css" href="css/contacte.css">
</head>
<body>
	<?php include 'php/header.php';?>
	<div class="body">
		<section>
			<h1>Contacta amb nosaltres!</h1>
			<div id="contacte">
				<p>El taller roman obert de Dilluns a Divendres de 8:00 a 20:00 hores.</p>
				<p>Totes les reserves es realitaran a través de la nostra pagina web dins l'horari indicat.</p>
				<p>Per qualsevol consulta contactan's via:</p>
				<p>Telefon: <strong>666999666</strong></p>
				<p>Email: <a href="mailto:">emailfalso123@gmail.com</a></p> 
			</div>

			<div class="mapa">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1779.917692925268!2d2.1069007871406065!3d41.38597163951796!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12a49854a99cad2f%3A0xb564263b0575d513!2sInstitut+Ausi%C3%A0s+Marc!5e0!3m2!1sca!2ses!4v1512127002900" width="100%" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
			</div>
		</section>
	</div>
	<?php include 'php/footer.php';?>
</body>
</html>