# ITV-JAL

Creat per:

* Larisa Petrea
* Jerome Lomio Llemos
* Arnau Royo Raso

## Pràctica Transversal 1: Inspecció Tècnica de Vehicles

   En aquesta pràctica utilitzarem tot l'aprés a classe i posarem tots els nostres coneixements a prova.
   
   Aquest projecte té com a objectiu crear una pàgina per a demanar cita a un taller per a passar la ITV al nostre cotxe
   Permetrà triar dia i hora, mostrant en tot moment quins dies están o no disponibles, tota aquesta informació es guardarà en una base de dades.
   
   * [Treball fet durant les hores](https://docs.google.com/a/iam.cat/spreadsheets/d/1LgiVTsZSLo6kBv7uY7D3S3hq5vn5p4taPKYTjVmIEl0/edit?usp=sharing)  
   
   * [Disseny de la pàgina (Moqup)](https://app.moqups.com/a14larpetpet@iam.cat/a3Cs7vAPdW/view)  
   * [Labs](http://labs.iam.cat/~a14larpetpet/itv-jal/index.php)
