<?php 
	session_start(); 
	$matricula = $_SESSION["matricula"];
	$data = $_POST["data"];

	$dia = $_POST["dia"];
	$hora = $_POST["hora"];
	$_SESSION["dia"] = $dia;
	$_SESSION["hora"] = $hora;
?>
<!DOCTYPE html>

<html>
<head>
	<title>Motors IAM</title>
	<link rel="stylesheet" type="text/css" href="css/dades.css">
</head>
<body>
	<?php include 'php/header.php';?> 
	<script type="text/javascript" src="js/valida_dades.js"></script>
	<div class="body">
		<section class="cita">
			<h2>DADES PERSONALS</h2>
			<div class="container">
				<div class="dades_personals">
					<form method="POST" action="resum_reserva.php">
							<input maxlength="20" type="text" name="nom" required placeholder="Nom"> <br>
							<input maxlength="20" type="text" name="cognom" required placeholder="Cognom"> <br>
							<input maxlength="40" type="email" name="email" required placeholder="Email"><br>
							<input maxlength="9"type="tel" name="telefon" required placeholder="Telèfon"><br>
							<input type="button" value="Tornar" onclick="history.back()">
							<input type="submit" name="submit" id="submit" value="Enviar">
					</form>
				</div>	
				<div class="dades_anteriors">
					<table>
						<tr>
							<th>Matricula:</th>
							<td><?php echo $matricula?></td>
						</tr>
						<tr>
							<th>Dia:</th>
							<td><?php echo $dia ?></td>
						</tr>
						<tr>
							<th>Hora:</th>
							<td><?php echo $hora ?></td>
						</tr>
					</table>
				</div>
			</div>
		</section>
	</div>
	<?php include 'php/footer.php';?>
</body>
</html> 