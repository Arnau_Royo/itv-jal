<!DOCTYPE html>
<html>
<head>
	<title>Motors IAM</title>
</head>
<body>
	<?php include 'php/header.php';?>
	<div class="body">
		<h1>Qui som?</h1>
		<p>A <strong>Motors IAM</strong> volem poder ajudar-lo de la manera més ràpida i eficient possible a pasar la seva <strong>ITV</strong> (Inspecció Técnica de Vehicle).</p>
		<p>Disposem d'una plantilla de professionals especialitzats en vehicles al seu servei.</p>
		<p>El nostre taller esta ubicat en una zona de fàcil accessibilitat.</p>
	</div>
	<?php include 'php/footer.php';?>
</body>
</html>