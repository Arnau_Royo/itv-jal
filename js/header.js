$(document).ready(initEvents);

function initEvents() {
  var file = document.location.href.match(/[^\/]+$/); //agafa el nom de l'arxiu actual
  if(file) {
  	$('a[href="'+file[0]+'"]').addClass("current"); //els links a la pagina actual tindran la class "current"
  }
  else {
    $('a[href="index.php"]').addClass("current");
  }
}
