$(document).ready(function(){
	var week = ["Diumenge", "Dilluns", "Dimarts", "Dimecres", "Dijous", "Divendres", "Dissabte"];
	var month = ["Gener", "Febrer", "Març", "Abril", "Maig", "Juny", "Juliol", "Agost", "Setembre", "Octubre", "Novembre", "Desembre"];

	var current = new Date();
	var d = new Date();
	function mostraCites() {
		var table = "";
		do {
			table += `
			<table>
				<tr>
					<th colspan='12'>`+week[d.getDay()]+` `+d.getDate()+`, `+month[d.getMonth()]+` del `+d.getFullYear()+`</th>
				</tr>
			`;
			/* Crea dos files diferents per les hores */
			for(i=0; i<2; i++) {
				table += "<tr>";
				for(j=8; j<20; j++) {
					table += "<td><a>";
					if(i==0) {
						table += j+":00</a>";
					}
					else {
						table += j+":30</a>";
					}
				}
				table += "</tr>";
			}
			
			table += "</table>";

			d.setDate(d.getDate()+1);
		} while(d.getMonth() == current.getMonth());
		

		return table;
	}
	$(".cites").html(mostraCites());
});