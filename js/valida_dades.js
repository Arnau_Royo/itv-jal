$(document).ready(function(){
	var error;
//Validació nom i cognom
	/* Keyup */
	$('input[name="nom"], input[name="cognom"]').keyup(function(){
		if(!$(this).val().match(/^[A-Za-z]\w*/)){
			$(this).css('border-color', '#FF7684');
			error = true;
		}
		else{
			$(this).css('border-color', '#D1D1D1');
			error = false;
		}
	});
	/* Focusout */
	$('input[name="nom"], input[name="cognom"]').keyup(function(){
		if(!$(this).val().match(/^[A-Za-z]\w*/)){
			$(this).css('border-color', '#FF7684');
			error = true;
		}
		else{
			$(this).css('border-color', '#D1D1D1');
			error = false;
		}
	});
  
//Validació email
	/* Keyup */
	$('input[name="email"]').keyup(function(){
		if(!$(this).val().match(/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/)){
			$(this).css('border-color', '#FF7684');
			error = true;
		}
		else{
			$(this).css('border-color' , '#D1D1D1');
			error = false;
		}
	});
	/* Focusout */
	$('input[name="email"]').focusout(function(){
		if(!$(this).val().match(/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/)){
			$(this).css('border-color', '#FF7684');
			error = true;
		}
		else{
			$(this).css('border-color' , '#D1D1D1');
			error = false;
		}
	});


//Validació telèfon
	/* Keyup */
	$('input[name="telefon"]').keyup(function(){
		if(!$(this).val().match(/^[69]\d{8}$/)){
			$(this).css('border-color', '#FF7684');
			error = true;
		}
		else {
			$(this).css('border-color', '#D1D1D1');
			error = false;
		}
	});
	/* Focusout */
	$('input[name="telefon"]').focusout(function(){
		if(!$(this).val().match(/^[69]\d{8}$/)){
			$(this).css('border-color', '#FF7684');
			error = true;
		}
		else {
			$(this).css('border-color', '#D1D1D1');
			error = false;
		}
	});

//Submit
	/* Error */
	$('#submit').click(function(event){
		if(error) {
			event.preventDefault();
		}
	});

});