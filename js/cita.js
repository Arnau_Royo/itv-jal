$(document).ready(function(){
	var error;
//Validació matricula
	/* Keyup */
	$('input[name="matricula"]').keyup(function(){
		if(!$(this).val().toUpperCase().match(/^([0-9]){4}([B-DF-HJ-NPR-TW-Z]+){3}/)){
			$(this).css('border-color', '#FF7684');
			error = true;
		}

		else {
			$(this).css('border-color' , '#D1D1D1');
			error = false;
		}
	});
	/* Focusout */
	$('input[name="matricula"]').focusout(function(){
		if(!$(this).val().toUpperCase().match(/^([0-9]){4}([B-DF-HJ-NPR-TW-Z]+){3}/)){
			$(this).css('border-color', '#FF7684');
			error = true;
		}

		else {
			$(this).css('border-color' , '#D1D1D1');
			error = false;
		}
	});
	$('#submit').click(function(event){
		if(error) {
			event.preventDefault();
		}
	});
});