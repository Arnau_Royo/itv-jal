$(document).ready(initEvents);
var week = ["Diumenge", "Dilluns", "Dimarts", "Dimecres", "Dijous", "Divendres", "Dissabte"];
var month = ["Gener", "Febrer", "Març", "Abril", "Maig", "Juny", "Juliol", "Agost", "Setembre", "Octubre", "Novembre", "Desembre"];

function initEvents() {

//Amaga el camp del dia seleccionat al carregar la pagina

	$('#dia_seleccionat').hide();
	$("#error").hide();

//Genera dia actual i introdueix a dades
	var d = new Date();
	var current = new Date();
	var dades;
	function generaDades(d) {
		dades = {
			data: d.getFullYear()+"-"+(d.getMonth()+1)+"-"+d.getDate(),
			dia: d.getDate(),
			setmana: d.getDay(),
			mes: d.getMonth(),
			any: d.getFullYear(),
			hora: null
		};
	}
	generaDades(d);

//Genera dia seleccionat
	/* A partir de "dades" */

	function generaDia(dades) {
		var table = `
		<table>
			<tr>
				<th colspan='12'>`+week[dades.setmana]+` `+dades.dia+`, `+month[dades.mes]+` del `+dades.any+`</th>
			</tr>
		`;
		/* Crea dos files diferents per les hores */
		for(i=0; i<2; i++) {
			table += "<tr>";
			for(j=8; j<20; j++) {
				table += "<td><a>";
				if(i==0) {
					table += j+":00</a>";
				}
				else {
					table += j+":30</a>";
				}
			}
			table += "</tr>";
		}
		
		table += "</table>";

		return table;
	}
	
//Genera calendari
	/* A partir de "dades" */

	function generaCalendari(dades) {
		
		var calendari = `
			<table>
				<tr>
					<th colspan="7">`+month[dades.mes]+` `+dades.any+`</th>
				</tr>
				<tr>
					<th>DL</th>
					<th>DM</th>
					<th>DX</th>
					<th>DJ</th>
					<th>DV</th>
					<th>DS</th>
					<th>DG</th>
				</tr>
		`;
		var date = new Date (dades.any, dades.mes, dades.dia);
		date.setDate(date.getDate()-date.getDate()+1);
		while(date.getDay()!=1) {
			date.setDate(date.getDate()-1);
		}
		/* Crea tots els dies deshabilitant els dies d'altres mesos i caps de setmana */
		for(var i=0; i<6; i++) {
			calendari += "<tr>";
			for(var j=0; j<7; j++) {
				calendari += "<td ";
				/* Other months */
				if(date.getMonth()!=dades.mes) {
					calendari += "class='disabled' ";
				}
				else {
					/* Weekends */
					if(date.getDay() == 0|| date.getDay() == 6) {
						calendari += "class='disabled' ";
					}
					/* Days after today */
					else if(date.getDate() < dades.dia) {
						calendari += "class='disabled' ";
					}
					else {
						/* Today */
						if(date.getDate() == dades.dia && dades.mes == current.getMonth() && dades.any == current.getFullYear()) {
							calendari += "class='disabled' ";
						}
					}	
				}
				calendari += ">";
				calendari += date.getDate()+"</td>"
				date.setDate(date.getDate()+1);
			}
			calendari += "</tr>";
		}
		calendari += "</table>";

		return calendari;

	}
	
	$("#calendari").html(generaCalendari(dades));

//Navegacio Calendari

	/* Mes anterior */
	$('#anterior').click(function(event){
		if(dades.mes == current.getMonth() && dades.any == current.getFullYear()) {
			event.preventDefault();
			$('#error').show();
		}
		else {
			if(dades.mes == 0) {
				d = new Date((dades.any-1)+"-12");
				if(d.getMonth() == current.getMonth() && d.getFullYear() == current.getFullYear()) {
					d.setDate(current.getDate());
				}
				generaDades(d);
				$("#calendari").html(generaCalendari(dades));
			}
			else {
				d = new Date(dades.any+"-"+(dades.mes));
				if(d.getMonth() == current.getMonth() && d.getFullYear() == current.getFullYear()) {
					d.setDate(current.getDate());
				}
				generaDades(d);
				$("#calendari").html(generaCalendari(dades));
			}
		}
	});

	/* Mes seguent */
	$('#seguent').click(function(){
		$("#error").hide();
		if(dades.mes == 11) {
			d = new Date((dades.any+1)+"-1");
			generaDades(d);
			$("#calendari").html(generaCalendari(dades));
		}
		else {
			d = new Date(dades.any+"-"+(dades.mes+2));
			generaDades(d);
			$("#calendari").html(generaCalendari(dades));
		}
	});

//Seleccio del dia

	$('#calendari').on('click', 'td', function() {
		$("#error").hide();
		if(!$(this).hasClass('disabled')) {
			d = new Date(dades.any+"-"+(dades.mes+1)+"-"+$(this).text());
			generaDades(d);
			$("#dia_seleccionat").html(generaDia(dades));
			/* Mostra el dia seleccionat */
			$('#dia_seleccionat').show('slow');
			$('#calendari td').removeClass("selected");
			$(this).addClass("selected");
		}
	});
	$('#dia_seleccionat').on("click", "a", function() {
		if(!$(this).hasClass('disabled')) {
			dades.hora = $(this).text();
			$('#dia_seleccionat a').removeClass("selected");
			$(this).addClass("selected");
		}
	});

//Submit

	$('#submit').click(function(event){
		/* Si no hi ha hora seleccionada no es pot fer submit */
		if(!dades.hora) {
			event.preventDefault();
		}
		/* Canvia els valors dels inputs ocults per a fer post amb php */
		else {
			var string = week[dades.setmana]+' '+dades.dia+', '+month[dades.mes]+' del '+dades.any;
			$('input[name="data"]').val(string);
			$('input[name="dia"]').val(dades.data);
			$('input[name="hora"]').val(dades.hora);
		}
	});

}