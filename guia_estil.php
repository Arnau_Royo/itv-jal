<!DOCTYPE html>
<html>
<head>
	<title>Motors IAM</title>
	<link rel="stylesheet" type="text/css" href="css/guia_estil.css">
</head>
<body>
	<?php include 'php/header.php';?>
	<div class="body">
		<section class="inici">
			<h1>Guia d'estil de Motors IAM</h1>
			<article>
				<h2>Colors</h2>
				<p>Els dos colors corporatius principals de Motors IAM son: <strong>#63629B</strong> amb text blanc i <strong>#F5B260</strong> amb text negre.</p>
				<div class="circle iam_blau">#63629B</div>
				<div class="circle iam_taronja">#F5B260</div>
				<p>S'utilitza <strong>#63629B</strong> per al header i el footer. I <strong>#F5B260</strong> per als hover del header i border del footer.</p>
				<p>En el cas de la pagina d'administrador l'ús d'aquests dos colors sera invertit.</p>
				<p>El color de fons del body es: <strong>rgba(255,255,255,0.5)</strong> i el color del text negre.</p>
				<div class="circle iam_body"></div>
			</article>
			<article>
				<h2>Posicionament</h2>
				<p>El fons de la pagina sera la mateixa que la del Campus IAM:</p>
				<img src="img/background.jpg" alt="background">
				<p>I el contingut té margin tant a l'esquerra com a la dreta de <strong>10%</strong>.</p>
				<p>El header tindra la imatge corporativa seguida de la barra de navegació.</p>
				<p>I en el footer tindra un <strong>border-top</strong> de <strong>2px</strong>, i contindra enllaços d'interés com la pàgina a la guia d'estil. Seguidament a la dreta les icones de les xarxes socials amb els respectius enllaços. Finalment a sota de tot del footer la llicencia.</p>
			</article>
			
		</section>
	</div>
	<?php include 'php/footer.php';?>
</body>
</html>