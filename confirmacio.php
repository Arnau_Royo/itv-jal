<?php 
	session_start(); 
	$matricula = $_SESSION["matricula"];
	$nom = $_SESSION["nom"];
	$cognom = $_SESSION["cognom"];
	$email = $_SESSION["email"];
	$telefon = $_SESSION["telefon"];
	$dia = $_SESSION['dia'];
	$hora = $_SESSION['hora'];
?>

<!DOCTYPE html>
<html>
<head>
	<title>Motors IAM</title>
	<link rel="stylesheet" type="text/css" href="css/cancel.css">
</head>
<body>
	<?php include 'php/header.php';?>
	<script type="text/javascript" src="js/index.js"></script>
	<div class="body">
    <div id="missatge">
			<!-- insertar.php inserta les dades a la base de dades i si tot esta correcte dona un missatge -->
			<?php include "php/insertar.php"; ?>
		</div>	
  </div>
  <?php include 'php/footer.php';?>
</body>
</html> 