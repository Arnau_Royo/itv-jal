#Git cheat sheet

## Iniciar git:
*$ git init*  

configuracio inicial:  

*$ git config --global [user.name|email]*  

clonar un repositori:  

*$ git clone [url]*

## Crear una branca:
*$ git checkout -b [name_of_your_new_branch]*  

change working branch:  

*$ git checkout [name_of_branch]*  

list of branch:  

*$ git branch*  


##Resolver issue amb una unica comanda:
*git commit -m "fix #1"*  



*$ git push --set-upstream origin [name_of_branch]*

## Merge branch to master:
*$ git checkout [name_of_branch]*  

si hi ha algun conflicte es podra resoldre dins la branca sense afectar al master:  

*$ git merge master*  

*$ git checkout master*  

al haver arreglat els possibles conflictes no hauria d'haver-hi problemes:  

*$ git merge [name_of_branch]*  

*$ git push -u origin master*

## Enllaços d'interes:

https://github.com/Kunena/Kunena-Forum/wiki/Create-a-new-branch-with-git-and-manage-branches  

https://stackoverflow.com/questions/14168677/merge-development-branch-with-master  

https://www.git-tower.com/blog/posts/git-cheat-sheet  
