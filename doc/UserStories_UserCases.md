# User Stories i User Cases


###Els user stories o histories d'usuari i els user cases o casos d’ús, presenten diferencies entre si, tot  I que la seva finalitat sigui la mateixa. Nosaltres però, usarem les histories d’usuari, que són més breus.



Utilitzarem el següent format: **com a** (qui es?) **vull**, (que vull?) **perquè** (perquè ho vull?)

**Com a** usuari **vull**, seleccionar el dia i hora, i saber que tenen lloc per mi **perquè** vull que el servei sigui àgil.  

**Com a** usuari **vull**, no haver-me de registrar per a fer una petició **perquè** amb la matrícula i el DNI en tenim prou.  

**Com a** administrador **vull**, veure ràpidament una vista de les revisions a fer les pròximes dues setmanes **perquè** això em permet organitzar el flux de treball al taller.  

**Com a** usuari **vull**, poder modificar l’hora i dia d’una cita **perquè** això em brinda molta flexibilitat. 

**Com a** usuari **vull**, poder seleccionar dies de mesos vinents **perquè** així no destorba en els meus plans. 

**Com a** administrador **vull**, poder posar-m’he en contacte amb els clients si escau **perquè** m’ajuda a informar d'algun canvi o incidència puntual als usuaris.  

**Com a** usuari **vull**, rebre un correu electrònic de confirmació amb l’hora i el dia **perquè** així estic ben segur de quan em toca.  

**Com a** usuari **vull**, una pàgina senzilla, on ràpidament sàpiga que he de fer, **perquè** vull que sigui user-friendly.  

**Com a** administrador **vull**, fer innacesible la meva pàgina de admin, sota una contrasenya **perquè** m'importa la seguretat.  

**Com a** administrador **no vull**, que l'usuari confongui el meu login amb el seu, que no n'hauria de tenir **perqué** els hi donarà molts mals de cap.  
