#GUIA D'INSTAL·LACIÓ
## Passos
1. Descarregar el sql que es troba en la carpeta /BD anomenat ITV_JAL.sql
2. Crear una base de dades i importar l'arxiu sql prèviament descarregat
3. Fer un git clone del nostre projecte del bitbucket:
	1. Per ssh: git clone git@bitbucket.org:Arnau_Royo/itv-jal.git
	2. Per https: git clone https://LarisaPetrea@bitbucket.org/Arnau_Royo/itv-jal.git
4. Donar els permisos necessaris per a la carpeta itv-jal que es crea
5. Modificar els fitxers que connecten amb la base de dades, canviar el nom de la bd, l'usuari i constrenya corresponens