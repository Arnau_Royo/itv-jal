<?php 
	session_start(); 
	$matricula = $_SESSION["matricula"];
	$error = false;
	//Validacio matricula //no hace bien la comprobacion
	/*$matricula = strtoupper($_POST["matricula"]);
	$pattern = "/^([0-9]){4}([B-DF-HJ-NPR-TW-Z]+){3}/";
	if(!preg_match($pattern, $matricula)) {
		$error = true;
	}
	else {
		$_SESSION["matricula"] = $matricula;
	} */
?>
<!DOCTYPE html>
<html>
<head>
	<title>Motors IAM</title>
	<meta charset="UTF-8">
	<link rel="stylesheet" type="text/css" href="css/escull_dia.css">
	
</head>
<body>
	<?php
	include 'php/header.php';
	if($error) {
		include 'php/error.php';
	}
	else { ?>
	<script type="text/javascript" src="js/escull_dia.js"></script>
	<div class="body">
		<form method="POST" action="introdueix_dades.php">
			<div>
				<a id="anterior" class="disabled">
					<span class="left"></span>
				</a>
				<section id="calendari"></section>
				<a id="seguent">
					<span class="right"></span>
				</a>
			</div>
			<section id="error">* No es poden seleccionar dates anteriors.</section>
			<section id="dia_seleccionat"></section>
			
			<section id="dades">
				<input type="text" name="data">
				<input type="text" name="dia">
				<input type="text" name="hora">
			</section>
			<input type="button" value="Tornar" onclick="history.back()">
			<input type="submit" name="submit" id="submit" value="Següent">
		</form>
	</div>
	<?php } 
	include 'php/footer.php'; ?>
</body>
</html>
