<?php 
	session_start();
	$error = false;

	$matricula = $_SESSION["matricula"];

//Validacio nom
	$nom = ucfirst(strtolower($_POST["nom"]));
	$pattern = "/^[A-Za-z]\w*/";
	if(!preg_match($pattern, $nom)) {
		$error = true;
	}
	else {
		$_SESSION["nom"] = $_POST["nom"];
	}

//Validacio cognom
	$cognom = ucfirst(strtolower($_POST["cognom"]));
	$pattern = "/^[A-Za-z]\w*/";
	if(!preg_match($pattern, $cognom)) {
		$error = true;
	}
	else {
		$_SESSION["cognom"] = $_POST["cognom"];
	}	
	
//Validacio email
	$email = $_POST["email"];
	$pattern = "/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/";
	if(!preg_match($pattern, $email)) {
		$error = true;
	}
	else {
		$_SESSION["email"] = $_POST["email"];
	}

//Validacio telefon
	$telefon = $_POST["telefon"];
	$pattern = "/^[69]\d{8}$/";
	if(!preg_match($pattern, $telefon)) {
		$error = true;
	}
	else {
		$_SESSION["telefon"] = $_POST["telefon"];
	}
	
	$dia = $_SESSION["dia"];
	$hora = $_SESSION["hora"];
/*
	$nom = $_POST["nom"];
	$cognom = $_POST["cognom"];
	$email = $_POST["email"];
	$telefon = $_POST["telefon"];
*/
?>
<!DOCTYPE html>

<html>
<head>
	<title>Motors IAM</title>
</head>
<body>
	<?php include 'php/header.php';?> 
	<script type="text/javascript" src="js/index.js"></script>
	<div class="body">
    <section class="resum">
      <h2>CONFIRMAR DADES</h2>
			<h4>DADES DE LA RESERVA</h4>
			<table>
				<tr>
					<th>Matrícula</th>
					<th>Nom</th>
					<th>Cognom</th>
					<th>Telèfon</th>
					<th>Email</th>
					<th>Dia</th>
					<th>Hora</th>
				</tr>
				<tr>
					<td><?php echo $matricula?></td>
					<td><?php echo $nom?></td>
					<td><?php echo $cognom?></td> 
					<td><?php echo $telefon?></td> 
					<td><?php echo $email?></td>
					<td><?php echo $dia?></td>
					<td><?php echo $hora?></td>
				</tr>
			</table>
			<form method="POST" action="confirmacio.php">
			<input type="button" value="Tornar" onclick="history.back()">
			<input type="submit" name="submit" value="Confirmar i reservar">

			</form>
    </section>
	</div>
    
    <?php include 'php/footer.php';?>
</body>
</html> 