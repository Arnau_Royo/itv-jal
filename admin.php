<!DOCTYPE html>
<html>
<head>
	<title>Admin Zone</title>
	<style type="text/css">
		.inici {
			text-align: center;
		}
	</style>
	<?php include 'php/header_admin.php';?>
</head>
<body>
	<div class="body">
		<section class="inici">
			<h1>Zona d'aministració</h1>
			<p>Seleccioneu dia per a veure les cites</p>
			<center>	
				<table>
					<tr>
						<th>Nom</th>
						<th>Cognom</th>
						<th>Email</th>
						<th>Telèfon</th>
						<th>Dia</th>
						<th>Hora</th>
						<th>Matrícula</th>
					</tr>
					<!-- inclou el php que dona els valors de la base de dades-->
					<?php include "php/dades.php"; ?>
				</table>
			</center>
		</section>
		<!-- calendari ? -->
		<section class="calendar">
			
		</section>
	</div>
	<?php include 'php/footer.php';?>
</body>
</html>
