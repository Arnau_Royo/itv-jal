<?php 
	session_start(); 
?>
<!DOCTYPE html>
<html>
<head>
	<title>Motors IAM</title>
</head>
<body>
	<?php include 'php/header.php';?>
	<script type="text/javascript" src="js/cita.js"></script>
	<div class="body">
		<section class="cita">
			<h2>Demanar cita</h2>
			<p>Aquí podeu demanar cita a Motors IAM.</p>
			<form method="POST" action="matricula_existent.php" id="matricula"> <!-- matricula_existent comprova si la matricula existeix o no a la base de dades -->
				<input maxlength="7" minlength="7" type="text" name="matricula" required placeholder="Introdueix la matrícula">
				<input type="submit" name="submit" id="submit" value="Següent">
			</form>
		</section>
	</div>
	<?php include 'php/footer.php';?>
</body>
</html>