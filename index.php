<!DOCTYPE html>
<html>
<head>
	<title>Motors IAM</title>
	<link rel="stylesheet" type="text/css" href="css/index.css">
</head>
<body>
	
	<?php include 'php/header.php';?>
	<div class="body">
		<section class="inici">
			<h1>Benvingut/da a Motors IAM</h1>
			<p>Pàgina per gestionar ITVs a Motors IAM</p>
			<form method="POST" action="matricula_existent.php" id="matricula">
				<input maxlength="7" minlength="7" type="text" name="matricula" required placeholder="Introdueix la matrícula">
				<input type="submit" name="submit" id="submit" value="Demanar cita">
			</form>
			<img src="img/itv.png">
		</section>
	</div>
	<?php include 'php/footer.php';?>
</body>
</html>
